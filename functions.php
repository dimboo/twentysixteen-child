<?php 
	
// Enqueue the parent and child theme stylesheets.
// The correct method of enqueuing the parent theme stylesheet is to add a wp_enqueue_scripts action and 
// use wp_enqueue_style() in your child theme's functions.php


	
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/rtl.css' );
}


/*

// Your child theme's stylesheet will usually be loaded automatically. If it is not, you will need to enqueue it as well. 
// Setting 'parent-style' as a dependency will ensure that the child theme stylesheet loads after it

function my_theme_enqueue_styles() {
    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

*/





	
?>